

mod compte {

    // La visibilité par défaut est par rapport au module
    // Les états peuvent accéder aux données internes des autres
    // états. Ceci ne cause aucun problème, car les utilisateurs de
    // ce code ne peuvent pas le voir.
    pub struct CompteInfo {
        username: String,
        password: String,
    }
    pub type Token = String;

    // type publique, mais constructeur privé
    pub struct NouveauCompte(CompteInfo);
    pub struct CompteConfirme(CompteInfo);
    pub struct CompteBloquer(CompteInfo);

    impl NouveauCompte {
        pub fn new(username: String, password: String) -> NouveauCompte {
            let ci = CompteInfo {
                username,
                password,
            };
            NouveauCompte(ci)
        }

        pub fn send_activation_email(&self) {
            return;
        }

        pub fn activate(self, _t: Token) -> CompteConfirme {
            CompteConfirme(self.0) // <-
        }
    }

    impl CompteConfirme {
        pub fn bloquer(self) -> CompteBloquer {
            CompteBloquer(self.0)
        }
    }

    impl CompteBloquer {
        pub fn debloquer(self, _t: Token) -> CompteConfirme {
            CompteConfirme(self.0) // <-
        }
    }
}

use compte::{NouveauCompte, Token};

fn main() {
    let token = String::from("token");
    let nouveau = NouveauCompte::new(String::from("louis"), 
                                     String::from("toto1234"));
    let confirme = nouveau.activate(token);
    // -- `nouveau` n'existe plus
    let _bloquer = confirme.bloquer();
    // -- `confirme` n'existe plus
    //confirme.bloquer();
}
