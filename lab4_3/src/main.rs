use std::rc::Rc;
use std::cell::{RefCell};
use std::fmt;

mod canvas {
    pub struct Canvas {
        items: Vec<Line>,
    }
    
    struct Line(f64,f64,f64,f64);

    impl Canvas {
    
        pub fn new() -> Canvas {
            Canvas {
                items: vec![],
            }
        }
        

        pub fn add_line(&mut self, x1:f64,y1:f64,x2:f64,y2:f64) {
            self.items.push(Line(x1,y1,x2,y2)); 
        }

        pub fn to_svg(&self) -> String {
            let mut res = String::from("<?xml version=\"1.0\" encoding=\"UTF-8\" ?> <svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" height=\"200\" width=\"500\">");
            
            for line in self.items.iter() {
                res.push_str(&line.to_svg());
                res.push('\n');
            }
            res.push_str("</svg>");
            return res;
        }
    }
    
    impl Line {
        pub fn to_svg(&self) -> String {
            format!("<line x1=\"{}\" y1=\"{}\" x2=\"{}\" y2=\"{}\" style=\"stroke:black;stroke-width:3\"/>",
                    self.0, self.1, self.2, self.3)
        }
    }
}

mod logo {
    use super::Rc;
    use super::{RefCell};
    use super::fmt;
    
    type LogoSeq<T: fmt::Debug> = Vec<Rc<Logo<T>>>;

    pub enum Logo<E: fmt::Debug = ()> {
        Forward(i32),
        Right(i32),
        Prog(LogoSeq<E>),
        Repeat(i32, Rc<RefCell<Logo<E>>>),
        Other(E),
    }
    

    impl<E: fmt::Debug> Logo<E> {

        pub fn fd_or_rt(&self) -> Option<&Logo<E>> {
            match self {
                Logo::Forward(_) => Some(self),
                Logo::Right(_) => Some(self),
                _ => None,
            }
        }

        pub fn new_fd(distance: i32) -> Logo<E> {
            Logo::Forward(distance)
        }
        pub fn new_rt(angle: i32) -> Logo<E> {
            Logo::Right(angle)
        }
        pub fn new_prog() -> Logo<E> {
            Logo::Prog(vec![])
        }
        pub fn new_rpt(number: i32) -> Logo<E> {
            let body = Rc::new(RefCell::new(Logo::new_prog()));
            Logo::Repeat(number, body) 
        } 

        pub fn fold(&self, f: &mut FnMut(&Logo<E>)) {
            f(self);
            match self {
                Logo::Prog(ref logos) => {
                    for l in logos {
                        l.fold(f);
                    }
                },
                Logo::Repeat(ref number, ref body) => {
                    for _ in 0..*number {
                        (*body.borrow()).fold(f);
                    }
                },
                _ => (),
            } 
        }
    }


    impl<T: fmt::Debug> fmt::Debug for Logo<T> {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            match self {
               Logo::Forward(distance) => write!(f, "FD {}", distance),
               Logo::Right(angle) => write!(f, "RT {}", angle),
               Logo::Prog(seq) => write!(f, "{:?}", seq),
               Logo::Repeat(number,seq) => write!(f, "REPEAT {} {:?}", number, seq.borrow()),
               Logo::Other(something) => write!(f, "{:?}", something),
            }
        }
    }

    pub struct LogoWriter<E: fmt::Debug = ()> {
        stack: Vec<Rc<RefCell<Logo<E>>>>,
    }

    impl<E: fmt::Debug> LogoWriter<E> {
        pub fn main(&mut self) -> Rc<RefCell<Logo<E>>> {
            self.stack.get(0).unwrap().clone()
        }

        pub fn add(self, logo: Rc<Logo<E>>) -> LogoWriter<E> {
            let last: &Rc<RefCell<Logo<E>>> = self.stack.last().unwrap();
            match *last.borrow_mut() {
                Logo::Prog(ref mut seq) => seq.push(logo),
                _ => (),
            }
            self
        }
        
        pub fn fd(self, distance: i32) -> LogoWriter<E> {
            self.add(Rc::new(Logo::new_fd(distance)))
        }

        pub fn rt(self, angle: i32) -> LogoWriter<E> {
            self.add(Rc::new(Logo::new_rt(angle)))
        }
        
        pub fn repeat(self, number: i32) -> LogoWriter<E> {
            let rpt = Rc::new(Logo::<E>::new_rpt(number));
            let mut self2 = self.add(rpt.clone());
            match *rpt {
                Logo::Repeat(_, ref body) => self2.stack.push(body.clone()), 
                _ => (),
            }
            self2
        }

        pub fn fini(mut self) -> LogoWriter<E> {
            self.stack.pop();
            self
        }
    }

    pub fn logo<E: fmt::Debug>() -> LogoWriter<E> {
        LogoWriter {
            stack: vec![Rc::new(RefCell::new(Logo::new_prog()))],
        } 
    }

    pub fn logo_star() -> Rc<RefCell<Logo>> {
        logo().repeat(5).fd(50).rt(144).fini().main()
    }

    pub fn logo_home<E: fmt::Debug>() -> Rc<RefCell<Logo<E>>> {
        logo().repeat(4).fd(50).rt(90).fini().fd(50).rt(-135).fd(35).rt(-90).fd(35).rt(225).main()
    }

}
mod turtle {
    use super::logo::*;
    use super::fmt;
    use std::f64::consts::PI;
    pub struct Turtle {
        pub x: f64,
        pub y: f64,
        pub angle: f64,
    }

    impl fmt::Debug for Turtle {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "({},{};{})", self.x, self.y, self.angle)
        }
    }

    impl Turtle {
        pub fn new(x:f64,y:f64,angle:f64) -> Turtle {
            Turtle {
                x,y,angle,
            }
        }
        pub fn fd(&mut self, distance: i32) {
            let angle_rad = self.angle * PI / 180.0;
            self.x += (distance as f64) * angle_rad.cos();
            self.y += (distance as f64) * angle_rad.sin();
        } 

        pub fn rt(&mut self, angle: i32) {
            self.angle += angle as f64;
            while self.angle >= 360.0 {
                self.angle -= 360.0;
            }
            while self.angle < 0.0 {
                self.angle += 360.0;
            }
        }
    }
    
    pub trait TurtleControl {
        fn apply(&self, turtle: &mut Turtle);
    }

    impl<E: fmt::Debug> TurtleControl for Logo<E> {
        fn apply(&self, turtle: &mut Turtle) {
            match self {
                Logo::Forward(distance) => turtle.fd(*distance),
                Logo::Right(angle) => turtle.rt(*angle),
                Logo::Prog(seq) => seq.iter().for_each(|logo| logo.apply(turtle)),
                Logo::Repeat(number,body) => { 
                    for _ in 0..*number { 
                        body.borrow().apply(turtle); 
                    }
                },
                _ => (),
            }
        } 
    }
    
}

mod bound {
    use super::logo::*;
    use super::turtle::*;
    use super::fmt;

    pub struct BoundBox {
        xmin: f64,
        ymin: f64,
        xmax: f64,
        ymax: f64,
    }
    
    impl fmt::Debug for BoundBox {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "({},{})--({},{})",self.xmin,self.ymin,self.xmax,self.ymax)
        }
    }
        
    impl BoundBox {
        pub fn new() -> BoundBox {
            BoundBox {
                xmin: std::f64::INFINITY,
                ymin: std::f64::INFINITY,
                xmax: std::f64::NEG_INFINITY,
                ymax: std::f64::NEG_INFINITY,
            }
        }
        pub fn update(&mut self, x: f64, y: f64)  {
            if x > self.xmax { self.xmax = x }
	    	if x < self.xmin { self.xmin = x }
		    if y > self.ymax { self.ymax = y }
		    if y < self.ymin { self.ymin = y }
        } 
    }
    

    pub struct BoundState {
        bounding_box: BoundBox,
        turtle: Turtle,
    }

    impl BoundState {
        pub fn new() -> BoundState {
            let inf = std::f64::INFINITY;
            BoundState {
                bounding_box: BoundBox::new(), 
                turtle: Turtle::new(0.0, 0.0, 0.0), 
            }
        }
    }
         
    pub trait BoundVisitable {
        fn bounding_box(&self) -> BoundBox;
    }

    impl<E: fmt::Debug> BoundVisitable for Logo<E> {
        fn bounding_box(&self) -> BoundBox {
            bounding_box(self)
        }
    }

    pub fn bounding_box<E: fmt::Debug>(logo: &Logo<E>) -> BoundBox {
        let mut bv = BoundState::new();
        let mut f = |logo: &Logo<E>| {
            logo.fd_or_rt().map(|logo| logo.apply(&mut bv.turtle));
            bv.bounding_box.update(bv.turtle.x, bv.turtle.y);
        };
        logo.fold(&mut f);
        bv.bounding_box
    } 
}



mod trace {
    use super::logo::*;
    use super::turtle::*;
    use super::canvas::*;
    
    pub trait TraceVisitable {
        fn to_svg(&self) -> String;
    }
    
    impl TraceVisitable for Logo {
        fn to_svg(&self) -> String {
            visit_trace(self)
        }
    }
   
    pub fn accept_trace<E: super::fmt::Debug>(logo: &Logo<E>, ts: &mut TraceState) {
        let x1 = ts.turtle.x;
        let y1 = ts.turtle.y;
        logo.fd_or_rt().map(|logo| logo.apply(&mut ts.turtle));
        match logo {
            Logo::Forward(dist) => {
                let x2 = ts.turtle.x;
                let y2 = ts.turtle.y;
                ts.canvas.add_line(x1,y1,x2,y2);
            },
            _ => (),
        }
    } 

    pub fn visit_trace(logo: &Logo) -> String {
        let mut ts = TraceState::new();
        let mut f = |logo: &Logo| {
           accept_trace(logo, &mut ts); 
        };
        logo.fold(&mut f);
        ts.canvas.to_svg()
    }

    pub struct TraceState {
        pub turtle: Turtle,
        pub canvas: Canvas,
    }

    impl TraceState {
        pub fn new() -> TraceState {
            TraceState {
                turtle: Turtle::new(250.0, 100.0, 0.0),
                canvas: Canvas::new(),
            }
        }
    }
}
mod pen {
    use super::logo::*;
    use super::turtle::*;
    use super::canvas::{Canvas};
    use super::trace;
    use super::{Rc,RefCell}; 
    use super::trace::{TraceVisitable, TraceState};

    use super::fmt;

    pub enum PenLogo {
        Up,
        Down,
    }
        
    pub struct TraceState2 {
        is_shadow: bool,
        inner: TraceState,
        shadow: TraceState,
    }
    
    impl TraceState2 {
        pub fn new() -> TraceState2 {
            TraceState2 {
                is_shadow: false,
                inner: trace::TraceState::new(),
                shadow: trace::TraceState::new(),
            }
        }
    }
    
    impl TraceVisitable for Logo<PenLogo> {
        fn to_svg(&self) -> String {
            visit_trace(self)
        }
    }
    
    fn sync_canvas(src: &TraceState, dest: &mut TraceState) {
        dest.turtle.x = src.turtle.x;
        dest.turtle.y = src.turtle.y;
    }

    pub fn accept_trace(logo: &Logo<PenLogo>, ts2: &mut TraceState2) {
        match logo {
            Logo::Other(pl) => {
                ts2.is_shadow = !ts2.is_shadow;
                match pl {
                    PenLogo::Up => sync_canvas(&ts2.inner, &mut ts2.shadow),
                    PenLogo::Down => sync_canvas(&ts2.shadow, &mut ts2.inner),
                }
            },
            _ => (),
        }
        let mut ts = &mut ts2.inner;
        if ts2.is_shadow {
            ts = &mut ts2.shadow;
        }
        trace::accept_trace(logo, &mut ts);
    }
    
    pub fn visit_trace(logo: &Logo<PenLogo>) -> String {
        let mut ts2 = TraceState2::new();
        let mut f = |logo: &Logo<PenLogo>| {
           accept_trace(logo, &mut ts2); 
        };
        logo.fold(&mut f);
        ts2.inner.canvas.to_svg()
    }

    impl fmt::Debug for PenLogo {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            match self {
                PenLogo::Up => write!(f, "PU"),
                PenLogo::Down => write!(f, "PD"),
            }
        }
    }

    pub trait PenLogoWriter {
        fn pu(self) -> LogoWriter<PenLogo>;
        fn pd(self) -> LogoWriter<PenLogo>;
    } 

    impl PenLogoWriter for LogoWriter<PenLogo> {
        fn pu(self) -> LogoWriter<PenLogo> {
            self.add(Rc::new(Logo::Other(PenLogo::Up)))
        }

        fn pd(self) -> LogoWriter<PenLogo> {
            self.add(Rc::new(Logo::Other(PenLogo::Down)))
        }
    }

    pub fn logo_town() -> Rc<RefCell<Logo<PenLogo>>> {
        let home = logo_home::<PenLogo>();
        let home = Rc::try_unwrap(home)
            .expect("failed to unwrap home")
            .into_inner();
        logo::<PenLogo>().pu().fd(-200).pd().repeat(7).add(Rc::new(home)).pu().fd(60).pd().main()
    }
}



use crate::logo::*;
use crate::turtle::*;
use crate::bound::*;
use crate::trace::*;
fn main() {
    // Etape 1
    use crate::logo::*;
    use crate::turtle::*;
    use crate::bound::*;
    use crate::pen::*;
    println!("{:?}", (logo_home::<()>().borrow()));
    println!("{:?}", (logo_star().borrow()));
    
    // Etape 2
    let mut t = Turtle::new(0.0,0.0,0.0);
    (logo::<()>().fd(50).main().borrow()).apply(&mut t);
    println!("{:?}", t);
    let mut t = Turtle::new(0.0,0.0,0.0);
    
    (logo::<()>().fd(10).rt(90).fd(20).main().borrow()).apply(&mut t);
    println!("{:?}", t);

    let mut t = Turtle::new(0.0,0.0,0.0);
    (logo::<()>().repeat(6).fd(5).repeat(5).fd(10).fini().rt(90).main().borrow()).apply(&mut t);
    println!("{:?}", t);

    let b = (logo::<()>().fd(10).main().borrow()).bounding_box();
    println!("{:?}", b);
    let b = (logo_star().borrow()).bounding_box();
    println!("{:?}", b);
    
    let b = (logo_home::<()>().borrow()).bounding_box();
    println!("{:?}", b);
    use std::fs;
    fs::write("home.svg", (logo_home::<()>().borrow()).to_svg()).expect("failed");
    fs::write("star.svg", (logo_star().borrow()).to_svg()).expect("failed");
    fs::write("town.svg", (logo_town().borrow()).to_svg()).expect("failed");
}


