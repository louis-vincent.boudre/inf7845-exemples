# INF7845 - Étude du langage Rust

Dans ce répertoire nous retrouvons les différents listings qui ont été présentés
mardi le 23 avril dans le cadre du cours INF7845. Le rapport et la présentation 
ci-retrouvent en format LaTex.

`auteur` : Louis-Vincent Boudreault

## Dépendances

- [rustup 1.33+](https://www.rust-lang.org/learn/get-started) : L'installeur de Rust qui permet d'installer
le compilateur (`rustc`) ainsi que le gestionnaire de paquets (`cargo`).


## Fonctionnement

Dans le répertoire, nous avons les programmes Rust suivant :

- `exemple_cyclic/`
- `exemple_lifetimes/`
- `exemple_ownership/`
- `graphe/`
- `lab2/`
- `lab4_1/`
- `lab4_2/`
- `lab4_3/`

Pour compiler et faire fonctionner chaque projet il suffit de faire la commande suivant :

```sh
$ cd <programme> && cargo run 
```

## Remarques

Les laboratoires 2 et 4 qui sont présentés dans la présentation sont codés de manières
à montrer étape par étape lors de la présentation. Si vous désirez comprendre le comportement
il faut décommenter les sections qu'il vous plait pour voir les effets sur le projet.  Certains
changement peuvent empêcher la compilation.

Le but étant de montrer les limites de Rust dans le paradigme objet. Cependant, dans le programme
`graphe/` le code est entièrement fonctionnel.


Pour le laboratoire 4, vous trouverez plusieurs version `lab4_X/`:
- `lab4_1/` : modélisation avec un `enum`
- `lab4_2/` : modélisation avec des `structs` et des `traits`
- `lab4_3/` : modéliastion avec un `enum` et un type générique

## State pattern en Rust

Dans le dossier `exemple_ownership/` nous retrouvons une implémentation du patron
_State_ en Rust. Ce dernier utilise l'ownership pour faire la transition entre les
états. Le point intéressant est qu'il est impossible de maintenir deux états en 
même temps grâce aux règles strictes du système d'ownership.

## graphe

Le programme qui traite les graphes s'inspirent des noms et certains choix de 
modélisation du projet [petgraph](https://docs.rs/petgraph/0.4.13/petgraph/). 
Cependant, l'implémentation reste totalement différente.

Le projet montre comment encoder des règles du domaines dans les types. L'exemple
que nous trouvons est au moment d'appeler dijkstra. Ce-dernier ne peut être 
appelé qu'avec des graphes ayant des poids numériques. Si vous tentez d'appeler
dijkstra avec un graphe ayant des strings comme poids, le code ne compilera pas.
D'où l'aspect intéressant de définir des règles qui peuvent être vérifiés à la
compilation.
