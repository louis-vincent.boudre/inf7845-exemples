use std::hash::Hash;
use std::collections::HashMap;
use std::collections::hash_map::{Keys, Iter};
use std::convert::From;
use std::marker::PhantomData;

use core::{
    GraphBase,
    IntoEdges,
    IntoNodes,
    NodeAdder,
    EdgeAdder,
    Directed,
    Undirected,
    RawEdges,
};


pub struct Graph<N: Hash + Eq + Clone, W: Clone, Es> {
    inner: HashMap<N, HashMap<N,W>>,
    phantom: PhantomData<Es>,
}

impl<N: Hash + Eq + Clone, W: Clone, Es> GraphBase for Graph<N,W,Es> {
    type Node=N;
    type Weight=W;
    type EdgeStyle=Es;
}

pub struct Edges<'a,N,W>
    where N: 'a + Hash + Eq + Clone,
          W: 'a + Clone,
{
    hmap: &'a HashMap<N,HashMap<N,W>>,
    keys: Option<Keys<'a, N, HashMap<N,W>>>,
    curr_key: Option<&'a N>,
    curr_entry: Option<Iter<'a, N, W>>,
}

pub struct Nodes<'a, N:'a + Hash + Eq + Clone, W: 'a + Clone>(Keys<'a, N, HashMap<N,W>>);

impl<'a, N, W> Iterator for Edges<'a,N,W> 
    where N: 'a + Hash + Eq + Clone,
          W: 'a + Clone,
{
    type Item = (&'a N, &'a N, &'a W);
    fn next(&mut self) -> Option<Self::Item> {
        if self.curr_key.is_none() {
            self.curr_key = self.keys.as_mut()
                .and_then(|ks| ks.next())
                .and_then(|next_key| {
                    self.curr_entry = self.hmap
                        .get(next_key)
                        .map(|entry| entry.iter());
                    return Some(next_key)
                });
            if self.curr_key.is_none() {
                return None;
            }
        }
        // Unwrap the key
        let u = self.curr_key.unwrap();
        if let Some((v,w)) = self.curr_entry.as_mut().unwrap().next() {
            self.curr_key = Some(u);
            return Some((u,v,w));
        } else {
            self.curr_key = None;
            return self.next();
        }
    }
}

impl<'a, N, W> Iterator for Nodes<'a,N,W> 
    where N: 'a + Hash + Eq + Clone,
          W: 'a + Clone,
{
    type Item = &'a N;
    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}

impl<'a,N,W,Es> IntoEdges<'a> for Graph<N,W,Es>
    where N: 'a + Hash + Eq + Clone, 
          W: 'a + Clone
{
    type Edge = (&'a N, &'a N, &'a W);
    type EdgeIter = Edges<'a, N, W>;
    fn edges(&'a self) -> Self::EdgeIter {
        self.iter()
    }

    fn edges_of(&'a self, u: &N) -> Self::EdgeIter {
        let curr_entry = self.inner.get(u).and_then(|e| Some(e.iter()));
        // Obligé de faire ça, car l'API hashmap = marde
        let curr_key = self.inner.keys().find(|k| k == &u); 
        Edges {
            hmap: &self.inner,
            keys: None,
            curr_key: curr_key,
            curr_entry: curr_entry,
        }
    }
}


impl<'a, N, W, Es> IntoNodes<'a> for Graph<N,W,Es> 
    where N: 'a + Hash + Eq + Clone,
          W: 'a + Clone,
          Es: 'a,
{
    type NodeIter = Nodes<'a, N, W>;
    fn nodes(&'a self) -> Self::NodeIter {
        self.nodes()
    } 
}


impl<N,W,Es> Graph<N,W,Es> 
    where N: Hash + Eq + Clone, 
          W: Clone,
{
    pub fn empty() -> Graph<N,W,Es> {
        Graph {
            inner: HashMap::new(),
            phantom: PhantomData,
        }
    }

    fn inner_add_edge(&mut self, edge: (N,N,W)) {
        let (u,v,w) = edge;
        self.add_node(v.clone()); 
        self.inner.entry(u)
            .or_insert(HashMap::new())
            .insert(v, w);
    }
    
    fn add_node(&mut self, node: N) {
        self.inner.entry(node)
            .or_insert(HashMap::new());
    }
    
    pub fn iter<'a>(&'a self) -> Edges<'a,N,W> {
        Edges {
            hmap: &self.inner,
            keys: Some(self.inner.keys()),
            curr_key: None,
            curr_entry: None,
        }
    }

    pub fn nodes<'a>(&'a self) -> Nodes<'a, N, W> {
        Nodes(self.inner.keys())
    } 
}

impl<N: Hash + Eq + Clone, W: Clone, Es> NodeAdder for Graph<N,W,Es> { 
    fn add_node(&mut self, node: Self::Node) {
        self.add_node(node); 
    }
}

impl<N: Hash + Eq + Clone, W: Clone> EdgeAdder for Graph<N,W,Directed> {
    fn add_edge(&mut self, edge: (N,N,W)) {
        self.inner_add_edge(edge); 
    } 
}

impl<N: Hash + Eq + Clone, W: Clone> EdgeAdder for Graph<N,W,Undirected> {
    fn add_edge(&mut self, edge: (N,N,W)) {
        let edge2 = (edge.1.clone(), edge.0.clone(), edge.2.clone());
        self.inner_add_edge(edge);
        self.inner_add_edge(edge2);
    } 
}

macro_rules! impl_from_raw_edges {
    (for $($t:ty),*) => ($(
        impl<N,W> From<RawEdges<N,W>> for Graph<N,W,$t> 
            where N: Hash + Eq + Clone,
                  W: Clone,
        {
            fn from(edges: RawEdges<N,W>) -> Graph<N,W,$t> {
                let mut g = Graph::empty(); 
                edges.into_iter().for_each(|edge| g.add_edge(edge));
                g
            } 
        }
    )*)
}
impl_from_raw_edges!(for Directed, Undirected);

/*
impl<N,W> From<Edges<N,W>> for Graph<N,W,Directed>
    where N: Hash + Eq + Clone, 
          W: Clone,
{
    fn from(edges: Edges<N,W>) -> Graph<N,W,Directed> {
        let mut g = Graph::empty(); 
        edges.into_iter().for_each(|edge| g.add_edge(edge));
        g
    } 
}

impl<N,W> From<Edges<N,W>> for Graph<N,W,Undirected>
    where N: Hash + Eq + Clone, 
          W: Clone,
{
    fn from(edges: Edges<N,W>) -> Graph<N,W, Undirected> {
        let mut g = Graph::empty(); 
        edges.into_iter().for_each(|edge| g.add_edge(edge));
        g
    } 
}
*/
