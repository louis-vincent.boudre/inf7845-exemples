use std::num::NonZeroUsize;
use std::marker::PhantomData;
use std::fmt::Debug;

pub trait Indexable: Eq + Clone + Default
{
    fn index(&self) -> usize;
}

use core::{
    GraphBase,
    IntoEdges,
    IntoNodes,
    EdgeAdder,
    Directed,
    Undirected,
    RawEdges,
};

pub struct VecGraph<N: Indexable, W: Eq + Clone, Es> {
    nodes: Vec<N>,
    inner: Vec<Vec<(N,W)>>,
    phantom: PhantomData<Es>,
}

impl<N: Indexable, W: Eq + Clone, Es> GraphBase for VecGraph<N,W,Es> {
    type Node=N;
    type Weight=W;
    type EdgeStyle=Es;
}


impl<'a,N,W,Es> IntoEdges<'a> for VecGraph<N,W,Es>
    where N: 'a+ Indexable, 
          W: 'a + Eq + Clone
{
    type Edge = (&'a N, &'a N, &'a W);
    type EdgeIter = Edges<'a, N, W>;
    fn edges(&'a self) -> Self::EdgeIter {
        self.iter()
    }

    fn edges_of(&'a self, u: &N) -> Self::EdgeIter {
        let i = u.index();
        Edges {
            nodes: &self.nodes[i..i+1],
            edges: &self.inner[i..i+1],
            node_idx: 0,
            edge_idx: 0,
        }
    }
}

impl<'a,N,W,Es> IntoNodes<'a> for VecGraph<N,W,Es>
    where N: 'a+ Indexable,
          W: 'a + Clone + Eq,
          Es: 'a,
{
    type NodeIter = Nodes<'a, N>;
    fn nodes(&'a self) -> Self::NodeIter {
        self.nodes() 
    }
}

pub struct Nodes<'a, N>
    where N: 'a + Indexable
{
    nodes: &'a [N],
    curr_idx: usize,
}

pub struct Edges<'a, N, W> 
    where N: 'a + Indexable,
          W: 'a + Eq + Clone,
{
    nodes: &'a [N],
    edges: &'a [Vec<(N,W)>],
    node_idx: usize,
    edge_idx: usize,
}

impl <'a, N> Iterator for Nodes<'a, N>
    where N: 'a + Indexable
{
    type Item = &'a N;
    fn next(&mut self) -> Option<(&'a N)> {
        let i = self.curr_idx;
        if i >= self.nodes.len() {
            None
        } else {
            self.curr_idx += 1;
            Some(&self.nodes[i])
        }
    }
}

impl<'a, N, W> Iterator for Edges<'a, N, W> 
    where N: 'a + Indexable,
          W: 'a + Eq + Clone,
{
    type Item = (&'a N, &'a N, &'a W);
    fn next(&mut self) -> Option<(&'a N, &'a N, &'a W)> {
        let (i,j) = (self.node_idx, self.edge_idx);
        if i >= self.nodes.len() {
            None
        } else {
            if self.edges[i].len() > j {
                self.edge_idx += 1;
                return Some((&self.nodes[i], 
                             &self.edges[i][j].0, 
                             &self.edges[i][j].1));
            } else {
                self.node_idx += 1;
                self.edge_idx = 0;
                self.next()
            } 
        }
    }
}

impl<N: Indexable, W: Eq + Clone, Es> VecGraph<N,W,Es> {
    pub fn with_capacity(capacity: NonZeroUsize) -> VecGraph<N,W,Es> {
        let cap = capacity.get();
        let mut vg = VecGraph {
            nodes: Vec::with_capacity(cap),
            inner: Vec::with_capacity(cap),
            phantom: PhantomData,
        };
        for _ in 0..cap {
            vg.nodes.push(N::default());
            vg.inner.push(vec![]);
        }
        vg
    }
    fn inner_add_edge(&mut self, edge: (N,N,W)) {
        let (u,v,w) = edge;
        let i: usize = u.index();
        let j: usize = v.index();
        self.nodes[i] = u;
        self.nodes[j] = v.clone();
        self.inner[i].push((v,w));
    }
   
    pub fn iter<'a>(&'a self) -> Edges<'a,N,W> {
        Edges {
            nodes: &self.nodes,
            edges: &self.inner,
            node_idx: 0,
            edge_idx: 0,
        }
    }

    pub fn nodes<'a>(&'a self) -> Nodes<'a, N> {
        Nodes {
            nodes: &self.nodes,
            curr_idx: 0,
        }
    }
}

impl<N: Indexable, W: Eq + Clone> EdgeAdder for VecGraph<N,W,Undirected> {
    fn add_edge(&mut self, edge: (N,N,W)) {
        let (u,v,w) = edge.clone();
        let edge2 = (v,u,w);
        self.inner_add_edge(edge);
        self.inner_add_edge(edge2);
    }
}
impl<N: Indexable, W: Eq + Clone> EdgeAdder for VecGraph<N,W,Directed> {
    fn add_edge(&mut self, edge: (N,N,W)) {
        self.inner_add_edge(edge);
    }
} 

macro_rules! impl_from_raw_edges {
    (for $($t:ty),*) => ($(
        impl<N, W> From<RawEdges<N,W>> for VecGraph<N,W,$t>         
            where N: Indexable + Debug,
                  W: Eq + Clone,
        {
            fn from(edges: RawEdges<N,W>) -> VecGraph<N,W,$t> {
                use std::cmp;
                let edges: Vec<(N,N,W)> = edges.into_iter().collect();

                let max = edges.iter()
                    .map(|(u,v,_)| cmp::max(u.index(), v.index()))
                    .max_by_key(|x: &usize| *x)
                    .expect("Only non zero sized edges are allowed");
                
                let capacity = NonZeroUsize::new(max+1).unwrap();
                let mut graph = VecGraph::with_capacity(capacity);
                edges.into_iter().for_each(|edge| graph.add_edge(edge));
                graph
            } 
        }
    )*)
}

impl_from_raw_edges!(for Directed, Undirected);

macro_rules! impl_indexable {
    (for $($t:ty),*) => ($(
        impl Indexable for $t {
            fn index(&self) -> usize {
                *self as usize
            }
        }
    )*)
}

impl_indexable!(for usize, u8, u16, u32, u64);

