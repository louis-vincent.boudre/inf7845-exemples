use std::collections::{HashMap};
use std::hash::Hash;
use std::cmp::Ordering;
use std::fmt::Debug;
use core:: {
    GraphBase,
    IntoNodes,
    IntoEdges,
    AbstractEdge,
};

pub type Color = usize;
//pub type Colors<'a, N> = Vec<(&'a N, Color)>;
pub type Colors<N> = Vec<(N, Color)>;

pub trait Colorable<'a>: GraphBase {
    fn greedy_coloring(&'a self) -> Colors<Self::Node>;
}


impl<'a, G> Colorable<'a> for G
    where G : IntoNodes<'a> + IntoEdges<'a>,
          G::Node: Debug,
{
    fn greedy_coloring(&'a self) -> Colors<Self::Node> {
        // Naive greedy algorithme
        let first = self.nodes().nth(0);
        if first.is_none() {
            return vec![];
        }
        
        let mut queue: Vec<Self::Node> = vec![first.unwrap().clone()];
        let mut colors: Colors<Self::Node> = vec![];
        let mut max_color = 1;

        while let Some(u) = queue.pop() {
            if colors.iter().any(|(v,_)| v ==&u) {
                continue;
            }
            let mut color_range: Vec<usize> = (1..max_color).collect();
            let neighbors_color: Vec<usize> = colors.iter()
                .filter(|(v,_)| self.has_edge(&u, v))
                .map(|(_,c)| *c)
                .collect();
            color_range.retain(|c| !neighbors_color.contains(c));
            let min_color = color_range.into_iter()
                .min()
                .unwrap_or(max_color + 1);
            if min_color > max_color {
                max_color = min_color
            }
            let ext: Vec<Self::Node> = self.edges_of(&u)
                .map(|edge| edge.end().clone()).collect();

            colors.push((u, min_color));
            queue.extend(ext);
        }
        colors
    }
}
