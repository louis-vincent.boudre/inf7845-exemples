extern crate num;
use self::num::{Num,Bounded};
use std::collections::{HashMap};
use std::hash::Hash;
use std::cmp::Ordering;
use core:: {
    GraphBase,
    IntoNodes,
    IntoEdges,
    AbstractEdge,
};

#[derive(Eq,Clone)]
struct X<V: Eq,W: Num + Bounded + Ord> {
    u: V,
    dist: W,
}

impl<V: Eq, W: Num + Bounded + Ord> PartialOrd for X<V,W> {
    fn partial_cmp(&self, other: &X<V,W>) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<V: Eq, W: Num + Bounded + Ord> PartialEq for X<V,W> {
    fn eq(&self, other: &X<V,W>) -> bool {
        self.dist == other.dist
    }
}

impl<V: Eq, W: Num + Bounded + Ord> Ord for X<V,W> {
    fn cmp(&self, other: &X<V,W>) -> Ordering {
        other.dist.cmp(&self.dist)
    }
}

pub trait ShortestPath<'a>: GraphBase {
    fn dijkstra(&'a self, source: Self::Node, end: Self::Node) -> Option<Self::Weight>;
}

impl<'a, G> ShortestPath<'a> for G
    where G: IntoNodes<'a> + IntoEdges<'a>,
          G::Node: Hash + Eq + Clone,
          G::Weight: Num + Bounded + Ord + Clone,
{
    fn dijkstra(&'a self, source: Self::Node, end: Self::Node) -> Option<Self::Weight> {
        use std::collections::{BinaryHeap};
        use self::num::Zero;

        let infinity = Self::Weight::max_value();
        let mut distances: HashMap<Self::Node, Self::Weight> = HashMap::new();
        for n in self.nodes() {
            distances.insert(n.clone(), infinity.clone());
        }

        let mut parent = HashMap::new();
        let mut queue = BinaryHeap::new();
        
        distances.insert(source.clone(), Self::Weight::zero());
        queue.push(X { u: source.clone(), dist: Self::Weight::zero() });

        while let Some(X { u, dist, }) = queue.pop() {
            if u == end { 
                return Some(dist);
            }
            let neighbors = self.edges_of(&u);
            for edge in neighbors {
                let v = edge.end().clone();
                let alt = dist.clone() + edge.weight().clone();
                let x = X{u: v.clone(), dist: alt.clone()};

                if alt < distances[&v] { 
                    queue.push(x);
                    distances.insert(v.clone(), alt);
                    parent.insert(v, u.clone());
                }
            } 
        }
        None 
    }
}
