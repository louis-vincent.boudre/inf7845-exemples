use std::convert::From;

pub mod backends;
pub mod algos;

/// Wrapper used for bulk insert of edges
/// This wrapper sanitized the edges before
/// building a graph.
pub struct RawEdges<N,W>(Vec<(N,N,W)>);


pub enum Directed {}
pub enum Undirected {}

pub trait AbstractEdge {
    type Node;
    type Weight;
    fn start(&self) -> &Self::Node;
    fn end(&self) -> &Self::Node;
    fn weight(&self) -> &Self::Weight;
}

pub trait GraphBase {
    type Node: Eq + Clone;
    type Weight: Clone;
    type EdgeStyle;
}

pub trait IntoEdges<'a>: GraphBase {
    type Edge: AbstractEdge<Node=Self::Node,Weight=Self::Weight>;
    type EdgeIter: Iterator< Item=Self::Edge >;
    fn edges(&'a self) -> Self::EdgeIter;
    fn edges_of(&'a self, u: &Self::Node) -> Self::EdgeIter;
    fn has_edge(&'a self, u: &Self::Node, v: &Self::Node) -> bool {
        self.edges_of(u).any(|edge| edge.end() == v)
    }
}

pub trait IntoNodes<'a> : GraphBase + 'a
{
    type NodeIter: Iterator<Item=&'a Self::Node> ;
    fn nodes(&'a self) -> Self::NodeIter;
}

pub trait NodeAdder: GraphBase {
    fn add_node(&mut self, node: Self::Node);
}

pub trait EdgeAdder: GraphBase {
    fn add_edge(&mut self, edge: (Self::Node, Self::Node, Self::Weight));
}

impl<'a , N, W> AbstractEdge for (&'a N, &'a N, &'a W) 
    where N: 'a + Clone,
          W: 'a + Clone,
{
    type Node = N;
    type Weight = W;
    fn start(&self) -> &Self::Node { self.0 }
    fn end(&self) -> &Self::Node { self.1 }
    fn weight(&self) -> &Self::Weight { self.2 }
}


impl<N: Eq, W: Eq> From<Vec<(N,N,W)>> for RawEdges<N,W> {
    fn from(mut edges: Vec<(N,N,W)>) -> RawEdges<N,W> {
        edges.dedup_by(|e1, e2| e1 == e2);
        RawEdges(edges)
    }
}

impl<N: Eq + Clone> From<Vec<(N,N)>> for RawEdges<N,()> {
    fn from(edges: Vec<(N,N)>) -> RawEdges<N,()> {
        let edges: Vec<(N,N,())> = edges.into_iter()
            .map(|(u,v)| (u,v,()))
            .collect();
        RawEdges::from(edges)
    }
}

impl<N,W> IntoIterator for RawEdges<N,W> {
    type Item = (N,N,W);
    type IntoIter = ::std::vec::IntoIter<(N,N,W)>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}


