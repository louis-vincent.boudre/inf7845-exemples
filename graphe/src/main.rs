mod core;
use core::algos::{ShortestPath, Colorable};
use core::backends::{Graph, VecGraph};

use core::{
    RawEdges, Directed, Undirected, IntoEdges
};


fn main() {
    let edges = vec![
        (0,2,10), 
        (0,1,1), 
        (1,3,2),
        (2,1,1),
        (2,3,3),
        (2,4,1),
        (3,0,7),
        (3,4,2)
    ];
    
    let edges2 = vec![
        ("A", "B"),
        ("A", "C"),
        ("B", "C"),
        ("B", "E"),
        ("B", "D"),
        ("C", "E"),
        ("C", "D"),
        ("D", "E"),
    ];
    
    let edges3 = vec![
        ("A", "B"),
        ("A", "J"),
        ("B", "E"),
        ("E", "F"),
        ("E", "G"),
        ("E", "C"),
        ("C", "D"),
        ("D", "G"),
        ("E", "G"),
        ("F", "H"),
        ("G", "H"),
        ("G", "I"),
        ("I", "J"),
    ];

    let graph1 = Graph::<u8, u8, Directed>::from(RawEdges::from(edges.clone()));
    let graph2 = VecGraph::<u8,u8, Directed>::from(RawEdges::from(edges.clone()));
  
    let graph3 = Graph::<&str, (), Undirected>::from(RawEdges::from(edges2));
    let graph4 = Graph::<&str, (), Undirected>::from(RawEdges::from(edges3));
    println!("Coloring g3: {:?}", graph3.greedy_coloring());
    println!("Coloring g4: {:?}", graph4.greedy_coloring());
    println!("sp1: {:?}", graph2.dijkstra(0,1));
    println!("sp2: {:?}", graph2.dijkstra(0,3));
    println!("sp3: {:?}", graph2.dijkstra(3,0));
    println!("sp4: {:?}", graph2.dijkstra(0,4));
    println!("sp5: {:?}", graph2.dijkstra(4,0));
   
    println!("------------------------"); 
    println!("sp1: {:?}", graph1.dijkstra(0,1));
    println!("sp2: {:?}", graph1.dijkstra(0,3));
    println!("sp3: {:?}", graph1.dijkstra(3,0));
    println!("sp4: {:?}", graph1.dijkstra(0,4));
    println!("sp5: {:?}", graph1.dijkstra(4,0));
}
