extern crate num;

mod graphe {
    use num::{Num, Bounded};
    use std::collections::HashMap;
    use std::hash::Hash;
    use std::convert::From;

    pub trait AbstractEdge {
        type Node;
        type Weight;
        fn start(&self) -> Self::Node;
        fn end(&self) -> Self::Node;
        fn weight(&self) -> Self::Weight;
    }
     
    pub trait GraphBase {
        type Node: Eq + Clone;
        type Edge: AbstractEdge;
    }
    
    pub trait IntoEdges: GraphBase {
        type EdgeIter: Iterator<Item=Self::Edge>;
        fn edges(&self) -> Self::EdgeIter;
    }
    
    pub trait IntoNodes: GraphBase {
        type NodeIter: Iterator<Item=Self::Node>;
        fn nodes(&self) -> Self::NodeIter;
    } 
    

    #[derive(Clone)]
    pub struct DefEdge<N: Clone, W: Clone>(N,N,W);

    impl<N: Clone, W: Clone> AbstractEdge for DefEdge<N,W> {
        type Node = N;
        type Weight = W;
        fn start(&self) -> Self::Node { self.0.clone() }
        fn end(&self) -> Self::Node { self.1.clone() }
        fn weight(&self) -> Self::Weight { self.2.clone() }
    }

    impl <'a, N: Clone, W: Clone> AbstractEdge for &'a DefEdge<N,W> {
        type Node = &'a N;
        type Weight = &'a W;
        fn start(&self) -> Self::Node { &self.0 }
        fn end(&self) -> Self::Node { &self.1 }
        fn weight(&self) -> Self::Weight { &self.2 }
    }

    struct Graph<N: Hash + Eq + Clone, W: Clone> {
        inner: HashMap<N, Vec<DefEdge<N,W>>>,
    }
    
    impl<N: Hash + Eq + Clone, W: Clone> GraphBase for Graph<N,W> {
            type Node=N;
            type Edge=DefEdge<N,W>;
    }

    impl<'a, G: GraphBase> GraphBase for &'a G
        where &'a G::Edge: AbstractEdge {
            type Node=G::Node;
            type Edge=&'a G::Edge;
    }

    impl<'a, N, W> IntoEdges for &'a Graph<N,W> 
        where N: 'a + Hash + Eq + Copy,
              W: 'a + Copy
    {
        type EdgeIter = std::vec::IntoIter<&'a DefEdge<N,W>>;
        fn edges(&self) -> Self::EdgeIter {
            self.inner_edges().into_iter()
        } 
    }

    impl<'a, N, W> IntoNodes for &'a Graph<N,W> 
        where N: Hash + Eq + Copy,
              W: Copy
    {
        type NodeIter = std::vec::IntoIter<N>;
        fn nodes(&self) -> Self::NodeIter {
            self.inner_nodes().into_iter()
        } 
    }

    impl<N,W> From<Vec<(N,N,W)>> for Graph<N,W>
        where N: Hash + Eq + Clone, 
              W: Clone,
    {
        fn from(edges: Vec<(N,N,W)>) -> Graph<N,W> {
            let Graph { inner: mut hmap } = Graph::empty(); 
            let iter = edges.into_iter()
                .map(|(u,v,w)| (u.clone(), DefEdge(u,v,w)));

            for (u,edge) in iter {
                hmap.entry(u)
                    .or_insert(vec![])
                    .push(edge)
            }
            Graph {
                inner: hmap,
            }
        } 
    }


    impl<N> From<Vec<(N,N)>> for Graph<N,()>
        where N: Hash + Eq + Clone, 
    {
        fn from(edges: Vec<(N,N)>) -> Graph<N,()> {
            let edges: Vec<(N,N,())> = edges
                .into_iter()
                .map(|(u,v)| (u,v,()))
                .collect();
            Graph::from(edges)
        } 
    }


    impl<N: Hash + Eq + Clone, W: Clone> Graph<N,W> {
    
        pub fn empty() -> Graph<N,W> {
            Graph {
                inner: HashMap::new(),
            }
        }

        fn inner_nodes(&self) -> Vec<N> {
            self.inner.keys()
                .cloned()
                .collect()
        } 

        fn inner_edges(&self) -> Vec<&DefEdge<N,W>>{
            self.inner.values()
                .flatten()
                .collect()
        }
    }
    /* 
    pub trait WeightedGraph: GraphBase {
        type Weight: Num + Bounded + Ord;
        fn dijkstra(&self, source: &Self::Node) -> Vec<Self::Node>; 
    }
    
    #[derive(Eq,Clone)]
    struct X<V: Eq,W: Num + Bounded + Ord> {
        u: V,
        dist: W,
    }

    impl<V: Eq, W: Num + Bounded + Ord> PartialOrd for X<V,W> {
        fn partial_cmp(&self, other: &X<V,W>) -> Option<Ordering> {
            Some(self.cmp(other))
        }
    }

    impl<V: Eq, W: Num + Bounded + Ord> PartialEq for X<V,W> {
        fn eq(&self, other: &X<V,W>) -> bool {
            self.dist == other.dist
        }
    }

    impl<V: Eq, W: Num + Bounded + Ord> Ord for X<V,W> {
        fn cmp(&self, other: &X<V,W>) -> Ordering {
            other.dist.cmp(&self.dist)
        }
    }

    impl<G> WeightedGraph for G
        where G: IntoEdges + IntoNodes,
              G::Node: Hash,
              <G::Edge as AbstractEdge>::Weight: Num + Bounded,
    {
        type Weight = <G::Edge as AbstractEdge>::Weight;

        fn dijkstra(&self, source: &Self::Node) -> Vec<Self::Node> {
            use std::collections::{BinaryHeap};
            use std::iter::FromIterator;
            use num::Zero;
            let mut distances: HashMap<Self::Node, Self::Weight> = 
                HashMap::from_iter(self.nodes().map(|v| (v.clone(), Self::Weight::max_value())));
            let mut parent = HashMap::new();
            let mut queue = BinaryHeap::new();
            
            distances.insert(source, Self::Weight::zero());
            queue.push(X { u: source, dist: Self::Weight::zero() });
            while let Some(X { u, dist }) = queue.pop() {
                if let Some(neighbors) = self.edges_of(&u) {
                    for edge in neighbors {
                        let v = edge.end();
                        let alt = dist + edge.weight();
                        let x = X{u: *v, dist: alt};

                        if alt < distances[v] {
                            queue.push(x);
                            distances.insert(*v, alt);
                            parent.insert(*v, u);
                        }
                    } 
                }
            }
            let mut curr = end;
            let mut path = vec![];
            while let Some(p) = parent.get(&curr) {
                path.push(*p);
                curr = *p;
            }
            path.reverse();
            path
            vec![]
        }
    }
    */
}



fn main() {
    println!("Hello, world!");
}
