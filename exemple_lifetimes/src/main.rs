use core::fmt::Debug;

// covariant sur 'a et T, car immuable
fn foo<'a>(x: &'a str, y: &'a str) {
    println!("{:?}, {:?}", x, y);
}

// Covariant sur 'a et invariant sur T, car mutable
fn foo2<'a, T: Debug>(x: &'a mut T, y: T) {
    *x = y;
}

struct Foo<In> {
    f: fn(In),
}

// Contravariance
fn exemple_contra<'short>(foo: &mut Foo<&'static u8>, g: fn(&'short u8)) {
    foo.f = g;
}

fn main() {
    // Covariance
    let mut x: &'static str = "Ma string static";
    {   // 'b
        let y: &str = &String::from("Ma string pas static");
        //let y2: &str = &y;  // coercition String -> slice str ~ &'b str
        //foo(y, x);        // foo(&&'static str, &&'b str) &'static <: &'b
        foo2(&mut x, y);
        /*
        let z = String::from("Ma string2 pas static");
        let mut z2: &str = &z;
        foo2(&mut z2, y2);      // foo(&&'static str, &&'b str) ==> &'static != &'b
        foo(z2,y2);
        */
    }
}

