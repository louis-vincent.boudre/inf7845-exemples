use std::rc::Rc;
use std::cell::{RefCell};
use std::fmt;

mod logo {
    use super::Rc;
    use super::{RefCell};
    use super::fmt;
    
    type LogoSeq = Vec<Rc<Logo>>;

    pub enum Logo {
        Forward(i32),
        Right(i32),
        Prog(LogoSeq),
        Repeat(i32, Rc<RefCell<Logo>>),
    }

    impl Logo {
        pub fn new_fd(distance: i32) -> Logo {
            Logo::Forward(distance)
        }
        pub fn new_rt(angle: i32) -> Logo {
            Logo::Right(angle)
        }
        pub fn new_prog() -> Logo {
            Logo::Prog(vec![])
        }
        pub fn new_rpt(number: i32) -> Logo {
            let body = Rc::new(RefCell::new(Logo::new_prog()));
            Logo::Repeat(number, body)
        }  
    }
    impl fmt::Debug for Logo {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            match self {
               Logo::Forward(distance) => write!(f, "FD {}", distance),
               Logo::Right(angle) => write!(f, "RT {}", angle),
               Logo::Prog(seq) => write!(f, "{:?}", seq),
               Logo::Repeat(number,seq) => write!(f, "REPEAT {} {:?}", number, seq.borrow()),
            }
            
        }
    }

    pub struct LogoWriter {
        stack: Vec<Rc<RefCell<Logo>>>,
    }

    impl LogoWriter {
        pub fn main(&mut self) -> Rc<RefCell<Logo>> {
            self.stack.get(0).unwrap().clone()
        }

        pub fn add(self, logo: Rc<Logo>) -> LogoWriter {
            //println!("printing: {:?}", logo);
            let last: &Rc<RefCell<Logo>> = self.stack.last().unwrap();
            match *last.borrow_mut() {
                Logo::Prog(ref mut seq) => seq.push(logo),
                _ => (),
            }
            self
        }
        
        pub fn fd(self, distance: i32) -> LogoWriter {
            self.add(Rc::new(Logo::new_fd(distance)))
        }

        pub fn rt(self, angle: i32) -> LogoWriter {
            self.add(Rc::new(Logo::new_rt(angle)))
        }
        
        pub fn repeat(self, number: i32) -> LogoWriter {
            let rpt = Rc::new(Logo::new_rpt(number));
            let mut self2 = self.add(rpt.clone());
            match *rpt {
                Logo::Repeat(_, ref body) => self2.stack.push(body.clone()), 
                _ => (),
            }
            self2
        }

        pub fn fini(mut self) -> LogoWriter {
            self.stack.pop();
            self
        }
    }

    pub fn logo() -> LogoWriter {
        LogoWriter {
            stack: vec![Rc::new(RefCell::new(Logo::new_prog()))],
        } 
    }

    pub fn logo_star() -> Rc<RefCell<Logo>> {
        logo().repeat(5).fd(50).rt(144).fini().main()
    }

    pub fn logo_home() -> Rc<RefCell<Logo>> {
        logo().repeat(4).fd(50).rt(90).fini().fd(50).rt(-135).fd(35).rt(-90).fd(35).rt(225).main()
    }
}

mod turtle {
    use super::logo::*;
    use super::fmt;
    use std::f64::consts::PI;
    pub struct Turtle {
        pub x: f64,
        pub y: f64,
        pub angle: f64,
    }

    impl fmt::Debug for Turtle {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "({},{};{})", self.x, self.y, self.angle)
        }
    }

    impl Turtle {
        pub fn new(x:f64,y:f64,angle:f64) -> Turtle {
            Turtle {
                x,y,angle,
            }
        }
        pub fn fd(&mut self, distance: i32) {
            let angle_rad = self.angle * PI / 180.0;
            self.x += (distance as f64) * angle_rad.cos();
            self.y += (distance as f64) * angle_rad.sin();
        } 

        pub fn rt(&mut self, angle: i32) {
            self.angle += angle as f64;
            while self.angle >= 360.0 {
                self.angle -= 360.0;
            }
            while self.angle < 0.0 {
                self.angle += 360.0;
            }
        }
    }
    
    pub trait TurtleControl {
        fn apply(&self, turtle: &mut Turtle);
    }

    impl TurtleControl for Logo {
        fn apply(&self, turtle: &mut Turtle) {
            match self {
                Logo::Forward(distance) => turtle.fd(*distance),
                Logo::Right(angle) => turtle.rt(*angle),
                Logo::Prog(seq) => seq.iter().for_each(|logo| logo.apply(turtle)),
                Logo::Repeat(number,body) => { 
                    for _ in 0..*number { 
                        body.borrow().apply(turtle); 
                    }
                },
            }
        } 
    }
    
}

mod visitor {
    use super::logo::*;

    pub trait Visitable {
        fn visit_all(&self, visitor: &mut Visitor);
    }

    impl Visitable for Logo {
        fn visit_all(&self, visitor: &mut Visitor) {
            match self {
                Logo::Prog(ref logos) => {
                    for l in logos {
                        visitor.visit(l);
                    }
                }
                Logo::Repeat(ref number, ref body) => {
                    for _ in 0..*number {
                        (*body.borrow()).visit_all(visitor);
                    }
                }
                _ => (),
            } 
        }
    } 
    
    pub trait Visitor {
        fn visit(&mut self, logo: &Logo);
    }
}

mod bound {
    
    use super::logo::*;
    use super::turtle::*;
    use super::visitor::*;
    use super::fmt;

    pub struct BoundBox {
        xmin: f64,
        ymin: f64,
        xmax: f64,
        ymax: f64,
    }
    
    impl fmt::Debug for BoundBox {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "({},{})--({},{})",self.xmin,self.ymin,self.xmax,self.ymax)
        }
    }
        
    impl BoundBox {
        pub fn new() -> BoundBox {
            BoundBox {
                xmin: std::f64::INFINITY,
                ymin: std::f64::INFINITY,
                xmax: std::f64::NEG_INFINITY,
                ymax: std::f64::NEG_INFINITY,
            }
        }
        pub fn update(&mut self, x: f64, y: f64)  {
            if x > self.xmax { self.xmax = x }
	    	if x < self.xmin { self.xmin = x }
		    if y > self.ymax { self.ymax = y }
		    if y < self.ymin { self.ymin = y }
        } 
    }

    pub struct BoundVisitor {
        bounding_box: BoundBox,
        turtle: Turtle,
    }

    impl BoundVisitor {
        pub fn new() -> BoundVisitor {
            let inf = std::f64::INFINITY;
            BoundVisitor {
                bounding_box: BoundBox::new(), 
                turtle: Turtle::new(0.0, 0.0, 0.0), 
            }
        }
    }
        
    pub trait LogoSingle {
        fn single(&self) -> Option<&Logo>;
    }
    pub trait BoundVisitable {
        fn bounding_box(&self) -> BoundBox;
        fn accept_bound(&self, bv: &mut BoundVisitor);
    }

    impl LogoSingle for Logo {
        fn single(&self) -> Option<&Logo> {
            match self {
                Logo::Forward(_) => Some(self),
                Logo::Right(_) => Some(self),
                _ => None,
            }
        }
    }

    impl BoundVisitable for Logo {
        fn bounding_box(&self) -> BoundBox {
            let mut v = BoundVisitor::new();
            v.visit(self);
            v.bounding_box
        }

        fn accept_bound(&self, bv: &mut BoundVisitor) {
            self.single().map(|logo| logo.apply(&mut bv.turtle));
        }
    }

    impl Visitor for BoundVisitor {
        fn visit(&mut self, logo: &Logo) {
            logo.accept_bound(self);
            self.bounding_box.update(self.turtle.x, self.turtle.y);
            logo.visit_all(self);
        }
    }
}

mod canvas {
    pub struct Canvas {
        items: Vec<Line>,
    }
    
    struct Line(f64,f64,f64,f64);

    impl Canvas {
    
        pub fn new() -> Canvas {
            Canvas {
                items: vec![],
            }
        }

        pub fn add_line(&mut self, x1:f64,y1:f64,x2:f64,y2:f64) {
            self.items.push(Line(x1,y1,x2,y2)); 
        }

        pub fn to_svg(&self) -> String {
            let mut res = String::from("<?xml version=\"1.0\" encoding=\"UTF-8\" ?> <svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" height=\"200\" width=\"500\">");
            
            for line in self.items.iter() {
                res.push_str(&line.to_svg());
                res.push('\n');
            }
            res.push_str("</svg>");
            return res;
        }
    }
    
    impl Line {
        pub fn to_svg(&self) -> String {
            format!("<line x1=\"{}\" y1=\"{}\" x2=\"{}\" y2=\"{}\" style=\"stroke:black;stroke-width:3\"/>",
                    self.0, self.1, self.2, self.3)
        }
    }
}

mod trace {
    use super::logo::*;
    use super::visitor::*;
    use super::turtle::*;
    use super::canvas::*;
    use super::bound::LogoSingle;
    pub trait TraceVisitable {
        fn to_svg(&self) -> String;
        fn accept_trace(&self, tv: &mut TraceVisitor);
    }
    
    impl TraceVisitable for Logo {
        fn to_svg(&self) -> String {
            let mut v = TraceVisitor::new();
            v.visit(self);
            v.canvas.to_svg()
        }

        fn accept_trace(&self, tv: &mut TraceVisitor) {
            let x1 = tv.turtle.x;
            let y1 = tv.turtle.y;
            self.single().map(|logo| logo.apply(&mut tv.turtle));
            match self {
                Logo::Forward(dist) => {
                    let x2 = tv.turtle.x;
                    let y2 = tv.turtle.y;
                    tv.canvas.add_line(x1,y1,x2,y2);
                },
                _ => (),
            }
        }
    }

    pub struct TraceVisitor {
        pub turtle: Turtle,
        pub canvas: Canvas,
    }

    impl TraceVisitor {
        pub fn new() -> TraceVisitor {
            TraceVisitor {
                turtle: Turtle::new(250.0, 100.0, 0.0),
                canvas: Canvas::new(),
            }
        }
    }

    impl Visitor for TraceVisitor {
        fn visit(&mut self, logo: &Logo) {
            logo.accept_trace(self);
            logo.visit_all(self);
        }
    }
}

mod pen {
    use super::logo::*;
    use super::visitor::*;
    use super::canvas::*;
    use super::trace;

    enum PenLogo {
        Base(Logo),
        Up,
        Down,
    }
    // ???
}

use crate::logo::*;
use crate::turtle::*;
use crate::visitor::*;
use crate::bound::*;
use crate::trace::*;

fn main() {
    // Etape 1
    println!("{:?}", (logo_home().borrow()));
    println!("{:?}", (logo_star().borrow()));
    
    // Etape 2
    let mut t = Turtle::new(0.0,0.0,0.0);
    (logo().fd(50).main().borrow()).apply(&mut t);
    println!("{:?}", t);
    
    let mut t = Turtle::new(0.0,0.0,0.0);
    (logo().fd(10).rt(90).fd(20).main().borrow()).apply(&mut t);
    println!("{:?}", t);

    let mut t = Turtle::new(0.0,0.0,0.0);
    (logo().repeat(6).fd(5).repeat(5).fd(10).fini().rt(90).main().borrow()).apply(&mut t);
    println!("{:?}", t);

    let b = (logo().fd(10).main().borrow()).bounding_box();
    println!("{:?}", b);

    let b = (logo_star().borrow()).bounding_box();
    println!("{:?}", b);
    
    let b = (logo_home().borrow()).bounding_box();
    println!("{:?}", b);
    
    use std::fs;

    fs::write("home.svg", (logo_home().borrow()).to_svg()).expect("failed");
    fs::write("star.svg", (logo_star().borrow()).to_svg()).expect("failed");
}
