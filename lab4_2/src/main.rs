use std::rc::Rc;
use std::cell::{RefCell};
use std::fmt;

mod logo {
    pub trait Visitor {
        fn visit_fd(&mut self, l: &Forward);
        fn visit_rt(&mut self, l: &Right);
        fn visit_prog(&mut self, l: &Prog);
        fn visit_rpt(&mut self, l: &Repeat);
    }

    use super::Rc;
    use super::{RefCell};
    use super::fmt;
    pub trait Logo: fmt::Debug {
        fn single(&self) -> Option<&Logo> {
            None
        }
        fn accept(&self, v: &mut Visitor);
    }

    type LogoSeq = Vec<Rc<Logo>>;
    pub struct Forward(pub i32);
    pub struct Right(pub i32);
    pub struct Prog(pub LogoSeq);
    pub struct Repeat(pub i32, pub Rc<RefCell<Prog>>);

    impl fmt::Debug for Forward {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "FD {}", self.0)
        }
    }
    
    impl fmt::Debug for Right {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "RT {}", self.0)
        }
    }

    impl fmt::Debug for Prog {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }

    impl fmt::Debug for Repeat {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "REPEAT {} {:?}", self.0, self.1.borrow())
        }
    }

    impl Logo for Forward {
        fn single(&self) -> Option<&Logo> {
            Some(self)
        }

        fn accept(&self, v: &mut Visitor) {
            v.visit_fd(&self);
        }
    }
    impl Logo for Right {
        fn single(&self) -> Option<&Logo> {
            Some(self)
        } 
        fn accept(&self, v: &mut Visitor) {
            v.visit_rt(&self);
        }       
    }
    impl Logo for Prog { 
        fn accept(&self, v: &mut Visitor) {
            v.visit_prog(&self);
        }
    }
    impl Logo for Repeat {
        fn accept(&self, v: &mut Visitor) {
            v.visit_rpt(&self);
        }
    }
    
    pub struct LogoWriter {
        stack: Vec<Rc<RefCell<Prog>>>,
    }

    impl LogoWriter {
        pub fn main(&mut self) -> Rc<RefCell<Prog>> {
            self.stack.get(0).unwrap().clone()
        }

        pub fn add(self, logo: Rc<Logo>) -> LogoWriter {
            let last: &Rc<RefCell<Prog>> = self.stack.last().unwrap();
            last.borrow_mut().0.push(logo);
            self
        }
        
        pub fn fd(self, distance: i32) -> LogoWriter {
            self.add(Rc::new(Forward(distance)))
        }

        pub fn rt(self, angle: i32) -> LogoWriter {
            self.add(Rc::new(Right(angle)))
        }
        
        pub fn repeat(self, number: i32) -> LogoWriter {
            let body = Rc::new(RefCell::new(Prog(vec![])));
            let rpt = Rc::new(Repeat(number, body.clone()));
            let mut self2 = self.add(rpt);
            self2.stack.push(body);
            self2
        }

        pub fn fini(mut self) -> LogoWriter {
            self.stack.pop();
            self
        }
    }

    pub fn logo() -> LogoWriter {
        LogoWriter {
            stack: vec![Rc::new(RefCell::new(Prog(vec![])))],
        } 
    }

    pub fn logo_star() -> Rc<RefCell<Logo>> {
        logo().repeat(5).fd(50).rt(144).fini().main()
    }

    pub fn logo_home() -> Rc<RefCell<Logo>> {
        logo().repeat(4).fd(50).rt(90).fini().fd(50).rt(-135).fd(35).rt(-90).fd(35).rt(225).main()
    }
       
}

mod turtle {
    use super::logo::*;
    use super::fmt;
    use std::f64::consts::PI;
    pub struct Turtle {
        pub x: f64,
        pub y: f64,
        pub angle: f64,
    }

    impl fmt::Debug for Turtle {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "({},{};{})", self.x, self.y, self.angle)
        }
    }

    impl Turtle {
        pub fn new(x:f64,y:f64,angle:f64) -> Turtle {
            Turtle {
                x,y,angle,
            }
        }
        pub fn fd(&mut self, distance: i32) {
            let angle_rad = self.angle * PI / 180.0;
            self.x += (distance as f64) * angle_rad.cos();
            self.y += (distance as f64) * angle_rad.sin();
        } 

        pub fn rt(&mut self, angle: i32) {
            self.angle += angle as f64;
            while self.angle >= 360.0 {
                self.angle -= 360.0;
            }
            while self.angle < 0.0 {
                self.angle += 360.0;
            }
        }
    }
    
    pub trait TurtleControl {
        fn apply(&self, turtle: &mut Turtle) {
            ()
        } 
    }
    
    impl TurtleControl for dyn Logo {
        fn apply(&self, turtle: &mut Turtle) {
            println!("dyn Logo::apply");
            //(*self).apply(turtle);
        }
    }

    impl TurtleControl for Forward {
        fn apply(&self, turtle: &mut Turtle) {
            turtle.fd(self.0);
        } 
    }

    impl TurtleControl for Right {
        fn apply(&self, turtle: &mut Turtle) {
            turtle.fd(self.0);
        } 
    }

    impl TurtleControl for Prog {
        fn apply(&self, turtle: &mut Turtle) {
            //self.0.iter().for_each(|logo| (logo).apply(turtle));
        } 
    }

    impl TurtleControl for Repeat {
        fn apply(&self, turtle: &mut Turtle) {
            (*self.1.borrow()).apply(turtle);
        } 
    }
    
    pub struct TurtleVisitor {
        turtle: Turtle,
    }
    impl TurtleVisitor {
        pub fn new() -> TurtleVisitor {
            TurtleVisitor {
                turtle: Turtle::new(0.0, 0.0, 0.0),
            }
        }

        pub fn into_turtle(self) -> Turtle {
            self.turtle
        }
    }
    
    impl Visitor for TurtleVisitor {
        fn visit_fd(&mut self, l: &Forward) {
            self.turtle.fd(l.0);
        }
        fn visit_rt(&mut self, l: &Right) {
            self.turtle.rt(l.0);
        }
        fn visit_prog(&mut self, l: &Prog) {
            l.0.iter().for_each(|logo| logo.accept(self));
        }
        fn visit_rpt(&mut self, l: &Repeat) {
            for _ in 0..l.0 {
                l.1.borrow().accept(self);
            } 
        }
    }
}


mod logo2 {
    use super::logo;
    pub use super::logo::{Logo,Forward,Right,Prog,Repeat};
    use super::fmt;

    pub struct Up;
    pub struct Down;

    pub trait Visitor2 {
        fn visit_fd(&mut self, l: &Forward);
        fn visit_rt(&mut self, l: &Right);
        fn visit_prog(&mut self, l: &Prog);
        fn visit_rpt(&mut self, l: &Repeat);
        fn visit_up(&mut self, l: &Up);
        fn visit_down(&mut self, l: &Down);
    }

    pub trait Logo2: Logo {
        fn accept(&self, v: &mut Visitor2);
    }

    impl fmt::Debug for Up {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "PU")
        }
    }

    impl fmt::Debug for Down {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "PU")
        }
    }
   
    impl Logo for Down {
         fn single(&self) -> Option<&Logo> {
            Some(self)
         }
         fn accept(&self, v: &mut logo::Visitor) {
            ()  // Nothing
         }
    }

    impl Logo for Up {
         fn single(&self) -> Option<&Logo> {
            Some(self)
         }
         fn accept(&self, v: &mut logo::Visitor) {
            ()  // Nothing
         }
    }
    
    impl Logo2 for Up {
        fn accept(&self, v: &mut Visitor2) {
            v.visit_up(self);
        }
    }
    impl Logo2 for Down {
        fn accept(&self, v: &mut Visitor2) {
            v.visit_down(self);
        }
    }
    impl Logo2 for Forward {
        fn accept(&self, v: &mut Visitor2) {
            v.visit_fd(self);
        }
    }
    impl Logo2 for Right {
        fn accept(&self, v: &mut Visitor2) {
            v.visit_rt(self);
        }
    }
    impl Logo2 for Prog {
        fn accept(&self, v: &mut Visitor2) {
            v.visit_prog(self);
        }
    }

    impl Logo2 for Repeat {
        fn accept(&self, v: &mut Visitor2) {
            v.visit_rpt(self);
        }
    }
}

mod turtle2 {
    use super::logo2::*;
    use super::turtle;
    pub use super::turtle::{Turtle};
    
    pub struct TurtleVisitor {
        turtle: Turtle,
    }
    
    impl Visitor2 for TurtleVisitor {
        fn visit_fd(&mut self, l: &Forward) {
            self.turtle.fd(l.0);
        }
        fn visit_rt(&mut self, l: &Right) {
            self.turtle.rt(l.0);
        }
        fn visit_prog(&mut self, l: &Prog) {
            //l.0.iter().for_each(|logo| logo.accept(self));
        }
        fn visit_rpt(&mut self, l: &Repeat) {
            for _ in 0..l.0 {
                //l.1.borrow().accept(self);
            } 
        }
        fn visit_up(&mut self, l: &Up) {
            println!("PEN UP");
        }
        fn visit_down(&mut self, l: &Down) {
            println!("Down UP");
        }
    } 
}


fn main() {
    use crate::logo::*;
    use crate::turtle::*;
    // Etape 1
    println!("{:?}", (logo_home().borrow()));
    println!("{:?}", (logo_star().borrow()));
    
    // Etape 2
    
    let mut t = Turtle::new(0.0, 0.0, 0.0);
    logo_home().borrow().apply(&mut t);
    
    /*
    let mut tv = TurtleVisitor::new();
    (logo().fd(50).main().borrow()).accept(&mut tv);
    println!("{:?}", tv.into_turtle());
    
    let mut tv = TurtleVisitor::new();
    (logo().fd(10).rt(90).fd(20).main().borrow()).accept(&mut tv);
    println!("{:?}", tv.into_turtle());

    let mut tv = TurtleVisitor::new();
    (logo().repeat(6).fd(5).repeat(5).fd(10).fini().rt(90).main().borrow()).accept(&mut tv);

    println!("{:?}", tv.into_turtle());
    */
    /*
    let b = (logo().fd(10).main().borrow()).bounding_box();
    println!("{:?}", b);

    let b = (logo_star().borrow()).bounding_box();
    println!("{:?}", b);
    
    let b = (logo_home().borrow()).bounding_box();
    println!("{:?}", b);
    
    use std::fs;

    fs::write("home.svg", (logo_home().borrow()).to_svg()).expect("failed");
    fs::write("star.svg", (logo_star().borrow()).to_svg()).expect("failed");
    */
}
