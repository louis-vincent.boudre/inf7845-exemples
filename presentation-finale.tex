\documentclass{beamer}
\usepackage{listings}
\usepackage[utf8]{inputenc}

\title{Le langage Rust}
\author{travail présenté \linebreak par \linebreak Louis-Vincent Boudreault}
\date{April 2019}

\begin{document}
\maketitle
\section{Introduction}
\setlength{\parskip}{1em}
\begin{frame}{Introduction}
Rust est un langage de programmation multi-paradigme et statiquement typé.\par
Aussi performant que du C/C++.\par
Développement de logiciel plus sûre au niveau mémoire.\par
Facilite le développement d'application concurrentiel et parallèle.
\end{frame}

\begin{frame}{Ownership}
Rust possède un système de types basé sur le ownership :
\begin{itemize}
    \item un seul propriétaire par ressource allouée (nos objets/types);
    \item détermine quand désallouer une ressource;
    \item introduit les déplacements d'objets (\textit{move semantic}).
\end{itemize}
Dans ce modèle, il est à priori impossible de modéliser des structures cycliques : 
graphes, listes doublement chaînées, etc.
\end{frame}

\begin{frame}[fragile]
\frametitle{Ownership}
\begin{lstlisting}[
    basicstyle=\footnotesize, %or \small or \footnotesize etc.
]
struct Noeud {
   data: u8,
   suivant: Option<Box<Noeud>>,
   precedent: Option<Box<Noeud>>,
}
fn main() {
    let mut n1 = Box::new(Noeud {
        data: 1,
        suivant: None,
        precedent: None,
    });
    let n2 = Box::new(Noeud {
        data: 2,
        suivant: None,
        precedent: None,
    });
    // Erreur de compilation
    n1.suivant = Some(n2);      // n2 moved here
    n2.precedent = Some(n1);    // cannot assign n2.precedent
}

\end{lstlisting}
\end{frame}

\begin{frame}{Ownership}
Pour avoir des structures cycliques il faut :
\begin{itemize}
	\item utiliser un compteur de référence : Rc\textless T\textgreater \space ou Arc\textless T\textgreater ;
	\item déplacer les règles d'emprunt à l'exécution : RefCell\textless T\textgreater ;
	\item possiblement revoir la modélisation.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Ownership}
\begin{lstlisting}[
    basicstyle=\footnotesize, %or \small or \footnotesize etc.
]
struct Noeud {
   data: u8,
   suivant: Option<Rc<RefCell<Noeud>>>,
   precedent: Option<Rc<RefCell<Noeud>>>,
}

fn main() {
    let n1 = Rc::new(RefCell::new(Noeud {
        data: 1,
        suivant: None,
        precedent: None,
    }));
    let n2 = Rc::new(RefCell::new(Noeud {
        data: 2,
        suivant: None,
        precedent: None,
    }));
    (*n1.borrow_mut()).suivant = Some(n2.clone());
    (*n2.borrow_mut()).precedent = Some(n1.clone());
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Lifetimes}
La gestion de la mémoire se fait à l'aide d'annotations de région de code nommé
\textit{lifetimes}. Ce sont des annotations de durées de vie que l'on ajoute à nos types pointeurs :
\begin{lstlisting}
fn foo<'a,'b>(x: &'a i32, y: &'b i32);
\end{lstlisting}
Elles permettent de valider la légitimité d'un pointeur, et ce, à la compilation.
\footnote{Region-Based Memory Management in Cyclone, par Dan Grossman, Greg Morrisett, Michaels Hicks et trois autres}
\footnote{Safe Manual Memory Management in Cyclone, par Nikhil Swamy, Michael Hicks et trois autres}
\end{frame}

\begin{frame}
\frametitle{Lifetimes}	
Dans un système à \textit{ownership}, les alias ne peuvent pas désallouer une ressource. Ce sont des emprunts temporaires faits au propriétaire courant.\par
Prévient la désallocation prématurée, la double désallocation et les \textit{dangling-pointer}/\textit{use after free}.\par
\end{frame}

\begin{frame}
\frametitle{Variance des Lifetimes}	
En Rust, il y a aucune notion de sous-typage pour les structs que l'on crée. 
Elle est réservée que pour les durées de vie:
\centerline{\textbf{'a \textless: 'b} ­$\Rightarrow$ \textbf{'a} dépasse \textbf{'b} (covariance)}
\centerline{\textbf{'b \textless: 'a} ­$\Rightarrow$ \textbf{'a} dépasse \textbf{'b} (contravariance)}
\centerline{\textbf{'static} dépassé tous les durées de vie}
Dans le cas des structures génériques : Conteneur\textless T\textgreater , T va suivre
les règles de variance s'il possède une durée de vie (récursif) :
\linebreak
\centerline{\textbf{Brochette\textless FruitAPeler\textgreater} N'EST PAS sous-type de \textbf{Brochette\textless Fruit\textgreater}}
\centerline{\textbf{Brochette\textless 'static Fruit\textgreater} EST sous-type de \textbf{Brochette\textless 'a Fruit\textgreater}}
\end{frame}


\begin{frame}[fragile]
\frametitle{Covariance}
\begin{lstlisting}[
    basicstyle=\footnotesize, %or \small or \footnotesize etc.
]
// covariant sur 'a
fn foo<'a>(x: &'a str, y: &'a str) {
    println!("{:?}, {:?}", x, y);
}
fn main() {
    let x: &'static str = "Ma string static";
    {   // 'b
        let y = String::from("Ma string pas static");
        let y2: &str = &y;  // &'b str
        // foo(&'b str, &'static str) &'static <: &'b
        foo(x,y2);
    }
}
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Invariance}
Il est à noter qu'une durée de vie est covariante ssi elle est appliquée
sur un type immuable (référence en lecture seule).
 
Dans le cas d'un accès en écriture entre deux pointeurs, ceux-ci doivent avoir
la même durée de vie.
\end{frame}

\begin{frame}[fragile]
\frametitle{Exemple invariance}
\begin{lstlisting}[
    basicstyle=\footnotesize, %or \small or \footnotesize etc.
]
// covariant sur 'a et invariant sur T
fn bar<'a, T: Debug>(x: &'a mut T, y: T) {
    *x = y;
}

fn main() {
    let mut x: &'static str = "Ma string static";
    {   // 'b
        let y = String::from("Ma string pas static");
        let y2: &str = &y;  // &'b str
        // bar(&&'static str, &&'b str) ==> &'static != &'b
        bar(&x,y2);	// Erreur de compilation!
    }
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Contravariance}
La contravariance se retrouve chez les types fonctionnels :
\begin{lstlisting}
struct Foo<In> {
    f: fn(In),
}
fn baz<'short>
(
	foo: &mut Foo<&'static u8>, 
	g: fn(&'short u8)
) 
{
    foo.f = g;
}
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Conclusion \textit{Ownership} et \textit{Lifetimes}}
En résumé, une région de code correspond à un \textit{scope}.\par
Les \textit{lifetimes} sont des noms que l'on attribue aux \textit{scopes}.\par
L'\textit{ownership} permet de déplacer une ressource d'une région à une autre. De plus, il permet de se déplacer entre les fils d'exécution.\par
L'\textit{ownership} va libérer une ressource lorsqu'il deviendra \textit{out-of-scope}
\end{frame}

\begin{frame}{Traits}
En Rust, les traits sont le seul mécanisme qui permet de définir des abstractions. 
\par
Ils correspondent à la description que l'on retrouve dans l'article : \textit{Traits. Composable Units of Behavior}, Nathanael Schärli, Stéphane Ducasse, Oscar Nierstrasz et Andrew P. Black.
\end{frame}

\begin{frame}{Traits}
Attribut général d'un trait :
\begin{itemize}
	\item Héritage multiple de traits
	\item Hiérarchie de traits aplatis				
	\item Composabilité
\end{itemize}
Spécifique à Rust :
\begin{itemize}
	\item Implémentation en couverture
	\item Sélection multiple de traits
	\item polymorphisme ad hoc
\end{itemize}
\end{frame}

\begin{frame}{Hiérarchie de trait}
L'héritage de trait = réutilisation de code + déclaration de dépendances.\par
Il n'y aucune spécialisation $\Rightarrow$ aucun conflit de valeur.\par
Cependant, il peut y avoir des conflits de noms : deux traits qui possèdent la même signature $\Rightarrow$ préfixer le site d'appel (à la C++).\par
Distinction entre propriété globale et locale $\Rightarrow$ inutile.
\end{frame}
\begin{frame}[fragile]
\frametitle{Relation de dépendance}
\begin{lstlisting}
trait X {}
trait Y: X {}

struct A;

impl Y for A {} // Erreur!
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Relation de dépendance}
\begin{lstlisting}
trait X {}
trait Y: X {}

struct A;

impl X for A {}
impl Y for A {}	// Ok!
\end{lstlisting}
\end{frame}


\begin{frame}{Hiérarchie aplatis}
Implémenter un trait pour un type est équivalent à définir les méthodes
directement dans la classe.\par
La hiérarchie disparait.
\end{frame}

\begin{frame}{Composabilité}
Comparer aux mixins (Ruby et Dart), les traits se composent mieux
$\Rightarrow$ aucune réécriture implicite\par
L'ordre de déclaration n'a pas d'importance.\par
Les implémentations sont décentralisées.
On peut ajouter du comportement sur :\par
\begin{itemize}
	\item Un type local (dans le module courant)
	\item Un type situé dans un notre module
	\item Un type étrangé (dans une librairie)
\end{itemize}
Similaire aux objets implicites (\textit{views}) en Scala.
\end{frame}

\begin{frame}[fragile]
\frametitle{implicit en Scala}
\begin{lstlisting}[language=Scala]
object IntService {
	implicit class IntSquares(x: Int) {
		def square: Int = {
			return x*x		
		}	
	}
}
...
scala> import IntService
scala> 3 squares
9
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{trait en Rust}
\begin{lstlisting}
trait Squarable: Mul<Self,Output=Self> {
	fn square(self): Self {
		x*x
	}
}
impl Squarable for i32 {}
let x = 3.square();
\end{lstlisting}
\end{frame}


\begin{frame}{Implémentation en Couverture}
Se rapproche du raffinement de classe en Nit (l'ajout de comportement uniquement).\par

Rust :
\begin{itemize}
	\item Types qui partagent des comportements communs
	\item Hétérogène
	\item Largeur
	\item Partitionnement
	\item Utilise des clauses logiques sur les types génériques.
\end{itemize}

Nit :
\begin{itemize}
	\item Classe et ses extensions
	\item Homogène
	\item Modification beaucoup plus fine
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Exemple}
\begin{lstlisting}
impl<G> ShortestPath for G
	where G: IntoNodes + IntoEdges,
	      G::Node: Hash,
	      G::Weight: Num + Bounded,
{
	// dijkstra...
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Sélection multiple de trait}
Nous pouvons implémenter plusieurs fois le même trait s'il y a
au moins un type générique : 
\begin{lstlisting}
type Vector3f = [f32;3];
impl ops::Add<Vector3f> for Vector3f {
    type Output = Vector3f;
    fn add(self, rhs: Bar) -> Vector3f {
        [
            self[0] + rhs[0],
            self[1] + rhs[1],
            self[2] + rhs[2]
        ]
    }
}
\end{lstlisting}
\end{frame}
\begin{frame}[fragile]
\frametitle{Sélection multiple de trait (suite)}
\begin{lstlisting}
impl ops::Add<f32> for Vector3f {
    type Output = Vector3f;
    fn add(self, rhs: f32) -> Vector3f {
	[rhs + self[0], rhs + self[1], rhs + self[2]]
    }
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Sélection multiple de trait (fin)}
La sélection se fait de manière statique, donc à la compilation. Il faut faire attention aux ambiguïtés :
\begin{lstlisting}

impl<T: TA> MonTrait for MonType<T>;
// Erreur Si TB <: TA
impl<T: TB> MonTrait for MonType<T>;	

impl<u32> MonTrait for MonType<u32>;
// Erreur
impl<u32> MonTrait for MonType<u32>; 	
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Polymorphisme ad hoc}
Les traits en Rust sont analogues aux \textit{Typeclasses} en Haskell.\par 
Ceux-ci permettent le polymorphisme ad hoc (surcharge statique) en permettant \textbf{d'abstraire} le comportement surchargé.\par

C'est le cas des opérations arithmétiques : *,+,/,=

Très utile pour les méthodes définies à partir de comportement surchargé\footnotemark.\par
\centerline{$squares (x,y,z) = (square \, x, square \, y, square \, z)$}

\footnotetext{\textit{How to make ad-hoc polymorphism less ad hoc}, Philip Wadler et Stephen Blott, Université de Glasgow}
\end{frame}

\begin{frame}{Fin}
Présenter mon module sur les graphes.

Revoir laboratoire 2 et 4.
\end{frame}

\begin{frame}{Questions?}
\end{frame}


\end{document}















