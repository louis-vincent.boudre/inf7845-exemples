trait Fruit {
    fn toto(&self) {
        println!("Fruit::toto");
    }
}
trait AsFruit {
    fn as_fruit(&self) -> &Fruit;
}

impl<F: Fruit> AsFruit for F {
    fn as_fruit(&self) -> &Fruit {
        self
    } 
}

trait FruitAPeler: Fruit + AsFruit {
    fn peler(&self);
}


struct Fraise;
struct Orange;
struct Banane;

impl Fruit for Fraise{}
impl Fruit for Orange{}
impl Fruit for Banane{}

impl FruitAPeler for Banane{
    fn peler(&self) {
        println!("peler banane");
    }
}

impl FruitAPeler for Orange { 
    fn peler(&self) {
        println!("peler orange");
    }
}


type Brochette<F> = Vec<F>;
trait TBrochette<F> {
    fn embrocher(&mut self, f: F);
    fn debrocher(&mut self) -> F;
    //fn compte_sloubifruit(&mut self) -> usize;
}

trait TBrochettePelable {
    fn pele_mele(&self); 
}

impl<F> TBrochette<F> for Brochette<F>{
    fn embrocher(&mut self, f: F) {
        self.push(f); 
    }

    fn debrocher(&mut self) -> F {
        self.pop().unwrap() 
    }
    
    /*
    fn compte_sloubifruit(&mut self) -> usize {
        3
    }
    */
}

/*
impl Fruit for Box<Fruit> {}
impl Fruit for Box<FruitAPeler> {}
impl FruitAPeler for Box<FruitAPeler> {
    fn peler(&self) {
        (**self).peler();
    }
}

impl<F: FruitAPeler> TBrochettePelable for Brochette<F> {
    fn pele_mele(&self) {
        for f in self {
            f.peler();
        } 
    }
}

fn main() {
    let mut b1: Brochette<Box<dyn FruitAPeler>> = vec![];
    let mut b2: Brochette<Banane> = vec![];
    let banane = Banane;
    let orange = Box::new(Orange);
    b1.embrocher(orange);
    b1.embrocher(Box::new(Banane));
    b2.embrocher(banane);
    b2.pele_mele();
    b1.compte_sloubifruit();
    b1.pele_mele();
    
    println!("Hello, world!");
}
*/

fn main() {
    let b1: Brochette<Banane> = vec![Banane, Banane];
    //let b3: Brochette<FruitAPeler> = vec![Orange, Banane];  // 1 Error: Doesn't have Size known at compile time

    // Polymorphisme classique que l'on reconnait avec liaison tardive : Trait Object
    //let b3: Brochette<&FruitAPeler> = vec![&Orange, &Banane]; // 2
    //let b4: Brochette<&Fruit> = b3;   // 3 il n'y pas de coercition entre trait + d'autres problèmes mémoire
    pele_mele1(&b1);
    //pele_mele1(&b3);  // 2
    //compte_fruit1(&b1);   // 4
    //compte_fruit1(&b3);   // 4

    //let mut b4: Brochette<&Fruit> = b3.into_iter().map(|f| f.as_fruit()).collect();
    //appel_a_peau2(&mut b4); 
}

/*
impl Fruit for &dyn FruitAPeler {}
impl FruitAPeler for &dyn FruitAPeler {
    fn peler(&self) {
        (**self).peler();
    }
}
*/

fn pele_mele1<F: FruitAPeler>(b: &Brochette<F>) {
    for f in b {
        f.peler();
    }
}

fn compte_fruit1<F: Fruit>(b: &Brochette<F>) -> usize {
    let mut i = 0;
    for _ in b {
        i += 1
    }
    i
}

/*
fn appel_a_peau1<F: Fruit>(b: &mut Brochette<F>) {
    b.push(Orange);
}

fn appel_a_peau2(b: &mut Brochette<&Fruit>) {
    b.push(&Orange); 
}
*/
