/*
struct Noeud {
   data: u8,
   suivant: Option<Box<Noeud>>,
   precedent: Option<Box<Noeud>>,
}
*/
use std::rc::{Rc};
use std::cell::{RefCell};
struct Noeud {
   data: u8,
   suivant: Option<Rc<RefCell<Noeud>>>,
   precedent: Option<Rc<RefCell<Noeud>>>,
}

fn main() {
    let n1 = Rc::new(RefCell::new(Noeud {
        data: 1,
        suivant: None,
        precedent: None,
    }));

    let n2 = Rc::new(RefCell::new(Noeud {
        data: 2,
        suivant: None,
        precedent: None,
    }));
    (*n1.borrow_mut()).suivant = Some(n2.clone());
    (*n2.borrow_mut()).precedent = Some(n1);
}
